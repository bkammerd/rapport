#!/usr/bin/env python3

from __future__ import print_function

import sys
import os
import traceback
import base64
import shutil
import re
import readline
from datetime import datetime
import time
import operator
from statistics import mean,stdev
from functools import reduce
from math import sqrt
import multiprocessing

experiments      = []
rapport_dir      = ""
interactive_mode = False

student_t_95 = {
  1  : 12.71,
  2  : 4.303,
  3  : 3.182,
  4  : 2.776,
  5  : 2.571,
  6  : 2.447,
  7  : 2.998,
  8  : 2.306,
  9  : 2.262,
  10 : 2.228,
  11 : 2.201,
  12 : 2.179,
  13 : 2.160,
  14 : 2.145,
  15 : 2.131,
  16 : 2.120,
  17 : 2.110,
  18 : 2.101,
  19 : 2.093,
  20 : 2.086
}

def geo_mean(iterable):
  return (reduce(operator.mul, iterable)) ** (1.0/len(iterable))

def safe_mean(iterable, default=None):
  if len(iterable) == 0:
    return default
  return mean(iterable)

def safe_geo_mean(iterable):
  if (0.0 in iterable):
    xxx = [(x+1) for x in iterable]
    val = (reduce(operator.mul, xxx)) ** (1.0/len(xxx))
    return (val-1)
  return (reduce(operator.mul, iterable)) ** (1.0/len(iterable))

def safe_div(num, den, default=0.0):
  return ( (float(num) / den) if den != 0 else default )

# get95CI is used to calculate a confidence interval for the difference between
# two means (basemean and expmean). The value returned can be used to express
# an interval [-ci, +ci], which defines the range that the difference between
# the means will fall between with 95% likelihood.
#
def get95CI(basemean, expmean, basestd, expstd, nruns):

  foo   = ( (basestd**2) / nruns) + ( (expstd**2) / nruns)
  s_x   = sqrt( foo )

  if nruns < 30:

    faz  = (( (basestd**2) / nruns) **2) / (nruns-1)
    fez  = (( (expstd**2)  / nruns) **2) / (nruns-1)
    n_df = int(round( ((foo**2) / (faz + fez)) ))

    ci = (student_t_95[n_df] * s_x)

  else:

    # assume normal distribution
    ci = 1.96 * s_x

  return ci

def B(foo):
    return bytes(foo.encode("utf-8"))

def my_hash(foo):
    h = 5381

    for c in foo:
        h = (((((h << 5) & 0xFFFFFFFF) + h) & 0xFFFFFFFF) + ord(c)) & 0xFFFFFFFF

    return h

def err(msg):
    sys.stdout.flush()
    sys.stderr.write("ERROR: {}\n".format(msg))
    sys.stderr.flush()
    exit(1)

class BadQueryException(Exception):
    pass

def params_string(params_dict):
    s = ""
    for key in sorted(params_dict.keys()):
        s += "#" + str(key) + "=" + str(params_dict[key])
    return s

def hash_params(params_dict):
    return my_hash(params_string(params_dict))

class MatDaemonProfile:
    def __init__(self):
        self.intervals = []

class Experiment:
    def __init__(self):
        self.path       = ""
        self.params     = {}
        self.outs       = []
        self.name       = "<unnamed>"
        self.md_profile = None

    def get_string(self):
        return params_string(self.params)

falsey = [ "false", "False", "FALSE", "no",  "No",  "NO",  "off", "Off", "OFF" ]
truthy = [ "true",  "True",  "TRUE",  "yes", "Yes", "YES", "on",  "On",  "ON"  ]

def read_name(name_path):
    if not os.path.exists(name_path):
        return "<unnamed>"

    name = None
    try:
        with open(name_path, "r") as f:
            name = f.read().strip()
    except EnvironmentError as E:
        err("'{}': {}".format(params_path, E[1]))

    return name

def set_name(exp, name):
    try:
        with open(exp.path + "/name", "w") as f:
            f.write(name)
    except EnvironmentError as E:
        err("'{}': {}".format(params_path, E[1]))
    exp.name = name

def read_params(params_path):
    params = {}

    params["prefetchers_disabled"] = False

    try:
        with open(params_path, "r") as f:
            nr  = 0
            key = ""
            val = ""

            for line in f:
                line = line.strip()

                if nr & 1:
                    val = line
                    params[key] = val
                else:
                    key = line
                nr += 1

    except EnvironmentError as E:
        err("'{}': {}".format(params_path, E[1]))

    for key, val in params.items():
        if val in falsey:
            params[key] = False
        elif val in truthy:
            params[key] = True
        else:
            try:
                params[key] = int(val)
            except:
                try:
                    params[key] = float(val)
                except: pass

    # Don't want these in the hash
    if "tier_1_capacity" in params:
        del params["tier_1_capacity"]
    if "tier_2_capacity" in params:
        del params["tier_2_capacity"]

    return params

def read_outs(params, exp_path):
    outs = {
        "mtime":           "<no result>",
        "peak_rss":        "<no result>",
        "peak_system_rss": "<no result>",
        "runtime":         "<no result>",
        "exit_status":     "<no result>",
        "avg_ddr_rd_bw":   "<no result>",
        "avg_ddr_wr_bw":   "<no result>",
        "avg_ddr_tot_bw":  "<no result>",
        "avg_pmm_rd_bw":   "<no result>",
        "avg_pmm_wr_bw":   "<no result>",
        "avg_pmm_tot_bw":  "<no result>",
        "avg_sys_rd_bw":   "<no result>",
        "avg_sys_wr_bw":   "<no result>",
        "avg_sys_tot_bw":  "<no result>",
        "peak_ddr_rd_bw":  "<no result>",
        "peak_ddr_wr_bw":  "<no result>",
        "peak_ddr_tot_bw": "<no result>",
        "peak_pmm_rd_bw":  "<no result>",
        "peak_pmm_wr_bw":  "<no result>",
        "peak_pmm_tot_bw": "<no result>",
        "peak_sys_rd_bw":  "<no result>",
        "peak_sys_wr_bw":  "<no result>",
        "peak_sys_tot_bw": "<no result>",
        "tot_ddr_traffic": "<no result>",
        "tot_pmm_traffic": "<no result>",
        "date":            "<no result>",
    }

    outs["date"] = datetime.fromtimestamp(os.path.getctime(exp_path))

    if os.path.isfile(exp_path + "/stdout"):
        mtime = os.path.getmtime(exp_path + "/stdout")
        outs["mtime"] = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(mtime))
        try:
            with open(exp_path + "/stdout", "r") as f:
                rss_prog  = re.compile("\s*Maximum resident set size[^\d]*(\d+)")
                time_prog = re.compile("\s*Elapsed \(wall clock\).*: (.*)$")
                exit_prog = re.compile("\s*Exit status: (.*)$")
                for line in f:
                    result = rss_prog.match(line)
                    if result:
                        outs["peak_rss"] = int(result.group(1)) / (1024.0 * 1024.0)
                        if "mpi_ranks" in params and params["mpi_ranks"] > 0:
                            outs["peak_rss"] = outs["peak_rss"] * float(params["mpi_ranks"])
                            continue

                    result = time_prog.match(line)
                    if result:
                        time_str   = result.group(1)
                        time_split = [ float(x) for x in time_str.split(':') ]
                        if len(time_split) == 2:
                            outs["runtime"] = (60.0 * time_split[0]) + time_split[1]
                            #outs["runtime"] = time_split[0] + (time_split[1] / 60.0)
                        elif len(time_split) == 3:
                            outs["runtime"] = ((60.0 * 60.0) * time_split[0]) + (60.0 * time_split[1]) + time_split[2]
                            #outs["runtime"] = (60.0 * time_split[0]) + time_split[1] + (time_split[2] / 60.0)
                        continue

                    result = exit_prog.match(line)
                    if result:
                        outs["exit_status"] = int(result.group(1))
                        continue

        except EnvironmentError as E:
            err(str(E))

    if os.path.isfile(exp_path + "/peak_rss"):
        try:
            with open(exp_path + "/peak_rss", "r") as f:
                for line in f:
                    outs["peak_rss"] = int(str(line)) / (1024.0*1024.0*1024.0)
                    break

        except EnvironmentError as E:
            err(str(E))

    if os.path.isfile(exp_path + "/peak_system_rss"):
        try:
            with open(exp_path + "/peak_system_rss", "r") as f:
                for line in f:
                    outs["peak_system_rss"] = int(str(line)) / 1000000000.0
                    break

        except EnvironmentError as E:
            err(str(E))

    if os.path.isfile(exp_path + "/pcm.txt"):
        try:
            with open(exp_path + "/pcm.txt", "r") as f:
                n               = 0
                avg_ddr_rd      = 0.0
                avg_ddr_wr      = 0.0
                avg_ddr_tot     = 0.0
                avg_pmm_rd      = 0.0
                avg_pmm_wr      = 0.0
                avg_pmm_tot     = 0.0
                peak_ddr_rd     = 0.0
                peak_ddr_wr     = 0.0
                peak_pmm_rd     = 0.0
                peak_pmm_wr     = 0.0
                tot_ddr_traffic = 0.0
                tot_pmm_traffic = 0.0
                cpu_energy      = 0.0
                dimm_energy     = 0.0

                skip = 0

                for line in f:
                    if line.startswith(" Core (SKT)"):
                        skip = 1
                    elif line.startswith(" SKT"):
                        if skip:
                            skip = 0
                        else:
                            words = line.split()

                            val = float(words[2])
                            avg_ddr_rd += val
                            if val > peak_ddr_rd:
                                peak_ddr_rd = val

                            val = float(words[3])
                            avg_ddr_wr += val
                            if val > peak_ddr_wr:
                                peak_ddr_wr = val

                            val = float(words[6])
                            avg_pmm_rd += val
                            if val > peak_pmm_rd:
                                peak_pmm_rd = val

                            val = float(words[7])
                            avg_pmm_wr += val
                            if val > peak_pmm_wr:
                                peak_pmm_wr = val

                            val = float(words[8])
                            cpu_energy += val

                            val = float(words[9])
                            dimm_energy += val

                            n += 1

                tot_ddr_traffic = avg_ddr_rd
                tot_pmm_traffic = avg_pmm_rd

#                 if n > 0:
#                     avg_ddr_tot = (avg_ddr_rd + avg_ddr_wr) / n
#                     avg_ddr_rd  = avg_ddr_rd / n
#                     avg_ddr_wr  = avg_ddr_wr / n
#                     avg_pmm_tot = (avg_pmm_rd + avg_pmm_wr) / n
#                     avg_pmm_rd  = avg_pmm_rd / n
#                     avg_pmm_wr  = avg_pmm_wr / n
                if outs["runtime"] > 0:
                    avg_ddr_tot = (avg_ddr_rd + avg_ddr_wr) / outs["runtime"]
                    avg_ddr_rd  = avg_ddr_rd / outs["runtime"]
                    avg_ddr_wr  = avg_ddr_wr / outs["runtime"]
                    avg_pmm_tot = (avg_pmm_rd + avg_pmm_wr) / outs["runtime"]
                    avg_pmm_rd  = avg_pmm_rd / outs["runtime"]
                    avg_pmm_wr  = avg_pmm_wr / outs["runtime"]

                outs["avg_ddr_rd_bw"]   = avg_ddr_rd
                outs["avg_ddr_wr_bw"]   = avg_ddr_wr
                outs["avg_ddr_tot_bw"]  = avg_ddr_tot
                outs["avg_pmm_rd_bw"]   = avg_pmm_rd
                outs["avg_pmm_wr_bw"]   = avg_pmm_wr
                outs["avg_pmm_tot_bw"]  = avg_pmm_tot
                outs["peak_ddr_rd_bw"]  = peak_ddr_rd
                outs["peak_ddr_wr_bw"]  = peak_ddr_wr
                outs["peak_pmm_rd_bw"]  = peak_pmm_rd
                outs["peak_pmm_wr_bw"]  = peak_pmm_wr
                outs["tot_ddr_traffic"] = tot_ddr_traffic
                outs["tot_pmm_traffic"] = tot_pmm_traffic
                outs["tot_traffic"]     = tot_ddr_traffic + tot_pmm_traffic
                outs["cpu_energy"]      = cpu_energy
                outs["dimm_energy"]     = dimm_energy
        except EnvironmentError as E:
            err(str(E))
    else:
        if os.path.isfile(exp_path + "/pcm_memory.txt"):
            try:
                with open(exp_path + "/pcm_memory.txt", "r") as f:
                    ddr_rd_prog = re.compile(".*System DRAM Read[^\d]*(\d+\.\d+)")
                    ddr_wr_prog = re.compile(".*System DRAM Write[^\d]*(\d+\.\d+)")
                    pmm_rd_prog = re.compile(".*System PMM Read[^\d]*(\d+\.\d+)")
                    pmm_wr_prog = re.compile(".*System PMM Write[^\d]*(\d+\.\d+)")

                    n            = 0
                    avg_ddr_rd   = 0.0
                    avg_ddr_wr   = 0.0
                    avg_ddr_tot  = 0.0
                    avg_pmm_rd   = 0.0
                    avg_pmm_wr   = 0.0
                    avg_pmm_tot  = 0.0
                    avg_sys_rd   = 0.0
                    avg_sys_wr   = 0.0
                    avg_sys_tot  = 0.0
                    peak_ddr_rd  = 0.0
                    peak_ddr_wr  = 0.0
                    peak_ddr_tot = 0.0
                    peak_pmm_rd  = 0.0
                    peak_pmm_wr  = 0.0
                    peak_pmm_tot = 0.0
                    peak_sys_rd  = 0.0
                    peak_sys_wr  = 0.0
                    peak_sys_tot = 0.0

                    ddr_rd = ddr_wr = pmm_rd = pmm_wr = -1.0
                    for line in f:
                        result = ddr_rd_prog.match(line)
                        if result:
                            ddr_rd = float(result.group(1)) / 1024.0
                            continue
                        result = ddr_wr_prog.match(line)
                        if result:
                            ddr_wr = float(result.group(1)) / 1024.0
                            continue
                        result = pmm_rd_prog.match(line)
                        if result:
                            pmm_rd = float(result.group(1)) / 1024.0
                            continue
                        result = pmm_wr_prog.match(line)
                        if result:
                            pmm_wr = float(result.group(1)) / 1024.0

                            if  (ddr_rd > -0.0001) and (ddr_wr > -0.0001) \
                            and (pmm_rd > -0.0001) and (pmm_wr > -0.0001):

                                sys_rd  = (ddr_rd + pmm_rd)
                                sys_wr  = (ddr_wr + pmm_wr)

                                ddr_tot = (ddr_rd + ddr_wr)
                                pmm_tot = (pmm_rd + pmm_wr)
                                sys_tot = (sys_rd + sys_wr)

                                avg_ddr_rd  += ddr_rd
                                avg_ddr_wr  += ddr_wr
                                avg_ddr_tot += ddr_tot
                                avg_pmm_rd  += pmm_rd
                                avg_pmm_wr  += pmm_wr
                                avg_pmm_tot += pmm_tot
                                avg_sys_rd  += sys_rd
                                avg_sys_wr  += sys_wr
                                avg_sys_tot += sys_tot

                                if ddr_rd > peak_ddr_rd:
                                    peak_ddr_rd = ddr_rd
                                if ddr_wr > peak_ddr_wr:
                                    peak_ddr_wr = ddr_wr
                                if ddr_tot > peak_ddr_tot:
                                    peak_ddr_tot = ddr_tot

                                if pmm_rd > peak_pmm_rd:
                                    peak_pmm_rd = pmm_rd
                                if pmm_wr > peak_pmm_wr:
                                    peak_pmm_wr = pmm_wr
                                if pmm_tot > peak_pmm_tot:
                                    peak_pmm_tot = pmm_tot

                                if sys_rd > peak_sys_rd:
                                    peak_sys_rd = sys_rd
                                if sys_wr > peak_sys_wr:
                                    peak_sys_wr = sys_wr
                                if sys_tot > peak_sys_tot:
                                    peak_sys_tot = sys_tot

                                n += 1

                            continue

                    outs["tot_ddr_traffic"]  = avg_ddr_rd
                    outs["tot_pmm_traffic"]  = avg_pmm_rd

                    if n > 0:
                        avg_ddr_rd  /= n
                        avg_ddr_wr  /= n
                        avg_ddr_tot /= n
                        avg_pmm_rd  /= n
                        avg_pmm_wr  /= n
                        avg_pmm_tot /= n
                        avg_sys_rd  /= n
                        avg_sys_wr  /= n
                        avg_sys_tot /= n

                    outs["avg_ddr_rd_bw"]   = avg_ddr_rd
                    outs["avg_ddr_wr_bw"]   = avg_ddr_wr
                    outs["avg_ddr_tot_bw"]  = avg_ddr_tot
                    outs["avg_pmm_rd_bw"]   = avg_pmm_rd
                    outs["avg_pmm_wr_bw"]   = avg_pmm_wr
                    outs["avg_pmm_tot_bw"]  = avg_pmm_tot
                    outs["avg_sys_rd_bw"]   = avg_sys_rd
                    outs["avg_sys_wr_bw"]   = avg_sys_wr
                    outs["avg_sys_tot_bw"]  = avg_sys_tot
                    outs["peak_ddr_rd_bw"]  = peak_ddr_rd
                    outs["peak_ddr_wr_bw"]  = peak_ddr_wr
                    outs["peak_ddr_tot_bw"] = peak_ddr_tot
                    outs["peak_pmm_rd_bw"]  = peak_pmm_rd
                    outs["peak_pmm_wr_bw"]  = peak_pmm_wr
                    outs["peak_pmm_tot_bw"] = peak_pmm_tot
                    outs["peak_sys_rd_bw"]  = peak_sys_rd
                    outs["peak_sys_wr_bw"]  = peak_sys_wr
                    outs["peak_sys_tot_bw"] = peak_sys_tot

            except EnvironmentError as E:
                err(str(E))

        if os.path.isfile(exp_path + "/pcm_power.txt"):
            try:
                with open(exp_path + "/pcm_power.txt", "r") as f:
                    cpu_joules  = 0.0
                    dram_joules = 0.0

                    cpu_joules_prog  = re.compile(".*Consumed Joules: (\d+\.\d+)")
                    dram_joules_prog = re.compile(".*Consumed DRAM Joules: (\d+\.\d+)")
                    for line in f:
                        result = cpu_joules_prog.match(line)
                        if result:
                            cpu_joules += float(result.group(1))
                            continue
                        result = dram_joules_prog.match(line)
                        if result:
                            dram_joules += float(result.group(1))
                            continue

                    outs["cpu_joules"]  = cpu_joules
                    outs["dram_joules"] = dram_joules

            except EnvironmentError as E:
                err(str(E))

    return outs

zero_proc = {
    'DDR_ACC':           0,
    'PMM_ACC':           0,
    'UNTRACKED_DDR_ACC': 0,
    'UNTRACKED_PMM_ACC': 0,
    'MISS_RATE_DDR':     0.0,
    'MISS_RATE_PMM':     0.0,
    'DDR_RSS':           0.0,
    'PMM_RSS':           0.0,
    'UNTRACKED_DDR_RSS': 0.0,
    'UNTRACKED_PMM_RSS': 0.0,
    'OBJECTS':           0,
}

def read_md_profile(exp_path):
    md_profile = MatDaemonProfile()

    if not os.path.isfile(exp_path + "/profile.txt"):
        return None

    try:
        with open(exp_path + "/profile.txt", "r") as f:
            procs = []
            proc  = zero_proc

            starts = {
                "DDR_ACC":           ("    DDR_",           int),
                "PMM_ACC":           ("    PMM_",           int),
                "UNTRACKED_DDR_ACC": ("    UNTRACKED DDR_", int),
                "UNTRACKED_PMM_ACC": ("    UNTRACKED PMM_", int),
                "MISS_RATE_DDR":     ("    MISS RATE DDR",  float),
                "MISS_RATE_PMM":     ("    MISS RATE PMM",  float),
                "DDR_RSS":           ("    DDR(",           float),
                "PMM_RSS":           ("    PMM(",           float),
                "UNTRACKED_DDR_RSS": ("    UNTRACKED DDR(", float),
                "UNTRACKED_PMM_RSS": ("    UNTRACKED PMM(", float),
                "OBJECTS":           ("    o",              int),
            }

            state                  = "interval"
            seen_interval          = False
            seen_proc_for_interval = False

            for line in f:
                if line.startswith("    0x"):
                    continue

                if line.startswith("p"):
                    state = "interval"
                    if seen_interval:
                        if seen_proc_for_interval:
                            procs.append(proc.copy())
                        md_profile.intervals.append(procs)
                        proc  = zero_proc
                        procs = []
                    seen_interval          = True
                    seen_proc_for_interval = False
                elif line.startswith("  P"):
                    state = "proc"
                    if seen_proc_for_interval:
                        procs.append(proc.copy())
                    proc                   = zero_proc
                    seen_proc_for_interval = True
                else:
                    if state == "proc":
                        for name, (start, ty) in starts.items():
                            if line.startswith(start):
                                proc[name] = ty(line.split()[-1])
                                break
                    else:
                        err("bad md_profile '{}'".format(exp_path + "/profile.txt"))

            if seen_interval:
                if seen_proc_for_interval:
                    procs.append(proc.copy())
                md_profile.intervals.append(procs)

    except EnvironmentError as E:
        err("'{}': {}".format(params_path, E[1]))

    return md_profile

def read_exp(exp_path):
    exp            = Experiment()
    exp.path       = exp_path
    exp.params     = read_params(exp_path + "/params")
    exp.outs       = read_outs(exp.params, exp_path)
    exp.name       = read_name(exp_path + "/name")
    exp.md_profile = None

    return exp

def update_progress(progress):
    barLength = 30
    status = ""
    if isinstance(progress, int):
        progress = float(progress)
    if not isinstance(progress, float):
        progress = 0
        status = "error: progress var must be float\r\n"
    if progress < 0:
        progress = 0
        status = "Halt...\r\n"
    if progress >= 1:
        progress = 1
        status = "Done.                  \r\n"
    block = int(round(barLength*progress))
    text = "\rPercent: [{0}] {1:.2f}% {2}".format( "#"*block + "-"*(barLength-block), progress*100, status)
    sys.stdout.write(text)
    sys.stdout.flush()

def load_one(path):
    if not os.path.isdir(path):
        return None
    return read_exp(path)

def load_exps():
    if not os.path.isdir(rapport_dir):
        err("directory '{}' does not exist!".format(rapport_dir))

    count = 0
    for fname in os.listdir(rapport_dir):
        path = rapport_dir + "/" + fname
        if not os.path.isdir(path):
            continue
        count += 1

    if count == 0:
        return

    it = 0
    if interactive_mode:
        print("Loading experiments...")
        update_progress(float(it)/float(count))

    with multiprocessing.Pool() as pool:
        for i in pool.imap(load_one, [ rapport_dir + "/" + x for x in os.listdir(rapport_dir) ]):
            if i != None:
                experiments.append(i)
            it += 1
            if interactive_mode:
                update_progress(float(it)/float(count))

def should_keep_exp(_q, _exp_idx):
    locals()["hash"] = os.path.basename(experiments[_exp_idx].path)

    for _key, _val in experiments[_exp_idx].params.items():
        locals()[_key] = _val
    for _key, _val in experiments[_exp_idx].outs.items():
        locals()[_key] = _val
    locals()["name"] = experiments[_exp_idx].name

    _keep = False

#    # MRJ: helps handle experiments that were run before a particular config
#    # option was created
#    #
#    if not _q.split()[0] in locals():
#        return False

    try:
        _keep = eval(_q)
    except (SyntaxError, NameError, TypeError) as S:
        raise BadQueryException("invalid query '{}'  ({})".format(_q, str(S)))
#        if isinstance(S, NameError):
#            _keep = False
#        else:
#            raise BadQueryException("invalid query '{}'  ({})".format(_q, str(S)))

    return _keep

def query(q, exp_indices):
    keep = []
    for i in exp_indices:
        if should_keep_exp(q, i):
            keep.append(i)
    return keep

def cmd_copydir():
    if len(sys.argv) < 3:
        err("expected directory path after 'copydir'")

    path = sys.argv[2]

    if not os.path.isdir(path):
        err("'{}' is not a directory".format(path))

    params_path = path + "/params"

    try:
        with open(params_path, "r") as f:
            params   = read_params(params_path)
            dir_name = base64.b64encode(B(str(hash_params(params))), B(".%"))
            dir_name = dir_name.replace(B('='), B('_')).decode("utf-8")
    except EnvironmentError as E:
        err("'{}': {}".format(params_path, E[1]))

    dir_path = rapport_dir + "/" + dir_name

    if os.path.isdir(dir_path):
        try:
            print("Overwriting '{}'.".format(dir_path))
            shutil.rmtree(dir_path, ignore_errors=True)
        except EnvironmentError as E:
            err("'{}': {}".format(dir_path, E[1]))

    try:
        shutil.copytree(path, dir_path)

    except EnvironmentError as E:
        err("'{}': {}".format(path, E[1]))

    print("Copied results to '{}'.".format(dir_path))

def do_list(exp_indices):
    for i in exp_indices:
        print(experiments[i].path)

def cmd_list():
    load_exps()

    queries = sys.argv[2:]

    exp_indices = range(0, len(experiments))

    for q in queries:
        exp_indices = query(q, exp_indices)

    do_list(exp_indices)

def do_csv(exp_indices, out_path):
    if len(exp_indices) == 0:
        return

    if not out_path or out_path == "":
        raise Exception("missing csv output path argument")

    e = 0
    if interactive_mode:
        print("Parsing profiles...")
        update_progress(float(e)/float(len(exp_indices)))
    for i in exp_indices:
        if experiments[i].params["profile_print_interval_period"] != 1:
            raise Exception("experiment '{}' has profile_print_interval_period != 1")
        if not experiments[i].md_profile:
            experiments[i].md_profile = read_md_profile(experiments[i].path)
            if not experiments[i].md_profile:
                raise Exception("experiment '{}' does not have a profile.txt".format(experiments[i].path))
        e += 1
        if interactive_mode:
            update_progress(float(e)/float(len(exp_indices)))

    max_intervals = max([ len(experiments[i].md_profile.intervals) for i in exp_indices ])

    proc_keys                = sorted(zero_proc.keys())
    exp_idx_to_max_num_procs = {}

    row = 0
    with open(out_path, "w") as f:
        header_string = "interval"
        e = 1;

        if interactive_mode:
            print("Writing rows...")
            update_progress(float(row)/float(max_intervals + 1))

        for idx in exp_indices:
            exp_idx_to_max_num_procs[idx] = 0
            for i in experiments[idx].md_profile.intervals:
                n_procs = len(i)
                if n_procs > exp_idx_to_max_num_procs[idx]:
                    exp_idx_to_max_num_procs[idx] = n_procs
            for p in range(1, exp_idx_to_max_num_procs[idx] + 1):
                header_string += ",E{}P{}".format(e, p)
                for key in proc_keys:
                    header_string += "," + str(key)
            header_string += ",,experiment,params"
            e += 1
        f.write(header_string)
        f.write("\n")
        row += 1

        for i in range(1, max_intervals + 1):
            if interactive_mode:
                update_progress(float(row)/float(max_intervals + 1))
            row_string = str(i)
            e = 1
            for idx in exp_indices:
                if len(experiments[idx].md_profile.intervals) >= i:
                    p = 1
                    for p_idx in range(0, exp_idx_to_max_num_procs[idx]):
                        row_string += ",E{}P{}".format(e, p)
                        if p_idx < len(experiments[idx].md_profile.intervals[i - 1]):
                            proc = experiments[idx].md_profile.intervals[i - 1][p_idx]
                            for key in proc_keys:
                                row_string += "," + str(proc[key])
                        else:
                            row_string += len(proc_keys) * ","
                        p += 1
                else:
                    p = 1
                    for p_idx in range(0, exp_idx_to_max_num_procs[idx]):
                        row_string += ",E{}P{}".format(e, p)
                        row_string += len(proc_keys) * ","
                        p += 1
                e += 1

            if i <= len(exp_indices):
                row_string += ",,E{},{}".format(i, params_string(experiments[exp_indices[i-1]].params))

            f.write(row_string)
            f.write("\n")
            row += 1

        if interactive_mode:
            update_progress(float(row)/float(max_intervals + 1))

def do_fullcsv(exp_indices, out_path):
    if len(exp_indices) == 0:
        return

    if not out_path or out_path == "":
        raise Exception("missing csv output path argument")

    import pandas as pd
    from tqdm import tqdm

    class Prof():
        def __init__(self):
            #header
            self.profile =             0
            self.time =                0
            self.pid =                 0
            self.ddr_acc =             0
            self.pmm_acc =             0
            self.untracked_ddr_acc =   0
            self.untracked_pmm_acc =   0
            self.ddr_alloc =           0
            self.ddr_free =            0
            self.pmm_alloc =           0
            self.pmm_free =            0
            self.ddr_mb =              0.0
            self.pmm_mb =              0.0
            self.untracked_ddr_mb =    0.0
            self.untracked_pmm_mb =    0.0
            self.objects =             0
            self.allocs =              0
            self.frees  =              0
            self.ignored_bytes =       0
            self.peak_rss =            0.0

            #last_row
            self.lr_object_range =     ""
            self.lr_hid =              0
            self.lr_creation_time =    0
            self.lr_last_access_time = 0
            self.lr_ddr_acc =          0.0
            self.lr_pmm_acc =          0.0
            self.lr_ddr_rss =          0.0
            self.lr_pmm_rss =          0.0

        def reset_row(self):
            self.profile =             0
            self.time =                0
            self.pid =                 0
            self.ddr_acc =             0
            self.pmm_acc =             0
            self.untracked_ddr_acc =   0
            self.untracked_pmm_acc =   0
            self.ddr_alloc =           0
            self.ddr_free =            0
            self.pmm_alloc =           0
            self.pmm_free =            0
            self.ddr_mb =              0.0
            self.pmm_mb =              0.0
            self.untracked_ddr_mb =    0.0
            self.untracked_pmm_mb =    0.0
            self.objects =             0
            self.allocs =              0
            self.frees =               0
            self.ignored_bytes =       0

        def read_header(self, lines, loc, which):
            for line in lines[loc:loc+18]:
                lline = line.split()
                if which == 0:
                    if lline[0] == "profile:":
                        self.profile = int(lline[1])
                    else:
                        print("loc:%d   BROKEN (profile)" % loc)
                        return 0

                    if lline[2] == "time(ms):":
                        self.time = int(lline[3])
                    else:
                        print("loc:%d   BROKEN (time(ms))" % loc)
                        return 0

                elif which == 1:
                    if lline[0] == "PID":
                        self.pid = int(lline[1])
                    else:
                        print("loc:%d   BROKEN (PID)" % loc)
                        return 0

                elif which == 2:
                    if lline[0] == "DDR_ACC:":
                        self.ddr_acc = int(lline[1])
                    else:
                        print("loc:%d   BROKEN (DDR_ACC)" % loc)
                        return 0

                elif which == 3:
                    if lline[0] == "PMM_ACC:":
                        self.pmm_acc = int(lline[1])
                    else:
                        print("loc:%d   BROKEN (PMM_ACC)" % loc)
                        return 0

                elif which == 4:
                    if  lline[0] == "UNTRACKED" and lline[1] == "DDR_ACC:":
                        self.untracked_ddr_acc = int(lline[2])
                    else:
                        print("loc:%d   BROKEN (UNTRACKED DDR_ACC)" % loc)
                        return 0

                elif which == 5:
                    if  lline[0] == "UNTRACKED" and lline[1] == "PMM_ACC:":
                        self.untracked_pmm_acc = int(lline[2])
                    else:
                        print("loc:%d   BROKEN (UNTRACKED PMM_ACC)" % loc)
                        return 0

                elif which == 6:
                    if lline[0] == "DDR_ALLOC:":
                        self.ddr_alloc = int(lline[1])
                    else:
                        print("loc:%d   BROKEN (DDR_ALLOC)" % loc)
                        return 0

                elif which == 7:
                    if lline[0] == "DDR_FREE:":
                        self.ddr_free = int(lline[1])
                    else:
                        print("loc:%d   BROKEN (DDR_FREE)" % loc)
                        return 0

                elif which == 8:
                    if lline[0] == "PMM_ALLOC:":
                        self.pmm_alloc = int(lline[1])
                    else:
                        print("loc:%d   BROKEN (PMM_ALLOC)" % loc)
                        return 0

                elif which == 9:
                    if lline[0] == "PMM_FREE:":
                        self.pmm_free = int(lline[1])
                    else:
                        print("loc:%d   BROKEN (PMM_FREE)" % loc)
                        return 0

                elif which == 10:
                    if lline[0] == "DDR(MB):":
                        self.ddr_mb = float(lline[1])
                    else:
                        print("loc:%d   BROKEN (DDR(MB))" % loc)
                        return 0

                elif which == 11:
                    if lline[0] == "PMM(MB):":
                        self.pmm_mb = float(lline[1])
                    else:
                        print("loc:%d   BROKEN (PMM(MB))" % loc)
                        return 0

                elif which == 12:
                    if lline[0] == "UNTRACKED" and lline[1] == "DDR(MB):":
                        self.untracked_ddr_mb = float(lline[2])
                    else:
                        print("loc:%d   BROKEN (UNTRACKED DDR(MB))" % loc)
                        return 0

                elif which == 13:
                    if lline[0] == "UNTRACKED" and lline[1] == "PMM(MB):":
                        self.untracked_ppm_mb = float(lline[2])
                    else:
                        print("loc:%d   BROKEN (UNTRACKED PMM(MB))" % loc)
                        return 0

                elif which == 14:
                    if lline[0] == "objects:":
                        self.objects = int(lline[1])
                    else:
                        print("loc:%d   BROKEN (objects)" % loc)
                        return 0
                elif which == 15:
                    if lline[0] == "allocs:":
                        self.allocs = int(lline[1])
                    else:
                        print("loc:%d   BROKEN (allocs)" % loc)
                        return 0
                elif which == 16:
                    if lline[0] == "frees:":
                        self.frees = int(lline[1])
                    else:
                        print("loc:%d   BROKEN (frees)" % loc)
                        return 0
                elif which == 17:
                    if lline[0] == "ignored":
                        self.ignored_bytes = int(lline[2])
                    else:
                        print("loc:%d   BROKEN (ignored bytes)" % loc)
                        return 0
                which += 1
            return 1

        def read_row(self, lines, loc):
            line =                     lines[loc]
            lline =                    line.split()

            self.lr_object_range =     lline[0]
            self.lr_hid =              int(lline[2])
            self.lr_creation_time =    int(lline[4])
            self.lr_last_access_time = int(lline[6])
            self.lr_ddr_acc =          float(lline[8])
            self.lr_pmm_acc =          float(lline[10])
            self.lr_ddr_rss =          float(lline[12])
            self.lr_pmm_rss =          float(lline[14])

        def add_row(self, df):
            column_str = ['profile', 'time', 'pid', 'ddr_acc', 'pmm_acc', 'untracked_ddr_acc',
                        'untracked_pmm_acc', 'ddr_alloc', 'ddr_free', 'pmm_alloc', 'pmm_free',
                        'ddr_mb', 'pmm_mb', 'untracked_ddr_mb', 'untracked_pmm_mb', 'objects',
                        'allocs', 'frees', 'ignored_bytes', 'object_range', 'object_hid', 'objet_creation_time',
                        'object_last_access_time', 'object_ddr_acc', 'object_pmm_acc',
                        'object_ddr_rss', 'object_pmm_rss', 'peak_rss']
            row = [[self.profile, self.time, self.pid, self.ddr_acc, self.pmm_acc, self.untracked_ddr_acc,
                self.untracked_pmm_acc, self.ddr_alloc, self.ddr_free, self.pmm_alloc, self.pmm_free,
                self.ddr_mb, self.pmm_mb, self.untracked_ddr_mb, self.untracked_pmm_mb, self.objects,
                self.allocs, self.frees, self.ignored_bytes, self.lr_object_range, self.lr_hid, self.lr_creation_time,
                self.lr_last_access_time, self.lr_ddr_acc, self.lr_pmm_acc, self.lr_ddr_rss, self.lr_pmm_rssi, self.peak_rss]]

            tmp_df = pd.DataFrame(row, columns = column_str)
            df     = df.append(tmp_df, ignore_index=True)
            return df

        def asdict(self):
            return {'profile': self.profile, 'time': self.time, 'pid': self.pid, 'ddr_acc': self.ddr_acc,
                    'pmm_acc': self.pmm_acc, 'untracked_ddr_acc': self.untracked_ddr_acc,
                    'untracked_pmm_acc': self.untracked_pmm_acc, 'ddr_alloc': self.ddr_alloc,
                    'ddr_free': self.ddr_free, 'pmm_alloc': self.pmm_alloc, 'pmm_free': self.pmm_free,
                    'ddr_mb': self.ddr_mb, 'pmm_mb': self.pmm_mb, 'untracked_ddr_mb': self.untracked_ddr_mb,
                    'untracked_pmm_mb': self.untracked_pmm_mb, 'objects': self.objects,
                    'allocs': self.allocs, 'frees': self.frees, 'ignored_bytes': self.ignored_bytes, 'object_range': self.lr_object_range,
                    'object_hid': self.lr_hid, 'objet_creation_time': self.lr_creation_time,
                    'object_last_access_time': self.lr_last_access_time, 'object_ddr_acc': self.lr_ddr_acc,
                    'object_pmm_acc': self.lr_pmm_acc, 'object_ddr_rss': self.lr_ddr_rss,
                    'object_pmm_rss': self.lr_pmm_rss, 'peak_rss': self.peak_rss}

    p = 0
    def prof_run(dir_loc, csv_name, time):
        file  = open(dir_loc, "r")
        lines = file.readlines()
        file.close()

        dict_list = []

        prof = Prof()
        prof.reset_row()

        loc =       0
        last_loc =  len(lines)
        first_row = []
        header_skip = 0
        prof.peak_rss = time

        for loc in tqdm(range(len(lines))):
            if header_skip:
                header_skip -= 1
                continue

            line = lines[loc]
            if (line.split())[0] == "profile:":
                if (loc+1 < last_loc) and (((lines[loc+1]).split())[0] == "PID"):
                    #add header
                    prof.read_header(lines, loc, 0)
                    if p:
                        print("Header loc:%d" % (loc + 1))
                        prof.print_header()
                        print("")
                    header_skip += 17
            elif (line.split())[0] == "PID":
                #add header
                prof.read_header(lines, loc, 1)
                if p:
                    print("Header loc:%d" % (loc + 1))
                    prof.print_header()
                    print("")
                header_skip += 16
            else:
                #has profile
                prof.read_row(lines, loc)
                if p:
                    print("Profile loc:%d" % (loc + 1))
                    prof.print_row()
                    print("")
            dict_data = prof.asdict()
            dict_list.append(dict_data)

        df = pd.DataFrame.from_dict(dict_list)
        df.to_csv((csv_name) + ".csv")

    if len(exp_indices) > 1:
        print("Please only make 1 csv at a time")
    else:
        tmp_ind  = exp_indices[0]
        exp_path = experiments[tmp_ind].path
        params   = experiments[tmp_ind].params
        outs     = read_outs(params, exp_path)
        prof_run(exp_path + "/profile.txt", out_path, outs['peak_rss'])

def cmd_csv():
    load_exps()

    args     = sys.argv[2:]
    out_path = ""
    if len(args) >= 1:
        out_path = args[0]

    if len(args) > 1:
        queries = args[1:]

    exp_indices = range(0, len(experiments))

    for q in queries:
        exp_indices = query(q, exp_indices)

    do_csv(exp_indices, out_path)

def _do_show(exp_indices, do_csv, *names):
    if len(exp_indices) == 0:
        return

    sep = "," if do_csv else "  "

    if len(names) == 0:
        names = [ "benchmark", "run_config", "reserve_perc", "input_size", "exit_status", "runtime", "mat_daemon", "policy_trigger_string", "policy_rank_string", "date" ]

    for name in names:
        if name == "hash" or name == "*":
            continue
        for i in exp_indices:
            if not name in experiments[i].params and \
            not name in experiments[i].outs and\
            not name == "name":
                experiments[i].params[name] = "<undefined>"
                #raise Exception("value '{}' does not exist for all selected experiments".format(name))

    if "*" in names:
        plist = list(experiments[exp_indices[0]].params.keys())
        olist = list(experiments[exp_indices[0]].outs.keys())
        names = set(plist + olist)
        for i in exp_indices:
            plist = list(experiments[i].params.keys())
            olist = list(experiments[i].outs.keys())
            kset  = set(plist + olist)
            names = names & kset
        names = sorted(list(names))

    def get_sort_val(key, idx):
        if key == "hash":
            return os.path.basename(experiments[idx].path)

        exp = experiments[idx]
        if key == "name":
            val = exp.name
        elif key in exp.params:
            val = exp.params[key]
            if val == "<undefined>":
                val = 0
        else:
            val = exp.outs[key]
            if val == "<no result>":
                val = 0

        return val

    def do_sort(names, exps):
        if len(names) == 0:
            return exps

        sorter = lambda idx: get_sort_val(names[0], idx)
        exps   = sorted(exps, key=sorter)

        i = 0
        while i < len(exps):
            val = get_sort_val(names[0], exps[i])
            j = i + 1
            while j < len(exps) and get_sort_val(names[0], exps[j]) == val:
                j += 1
            exps[i:j] = do_sort(names[1:], exps[i:j])
            i = j

        return exps

    exp_indices = do_sort(names, exp_indices)

    col_maxes = []
    for name in names:
        max = len(name)
        for i in exp_indices:
            if name == "hash":
                val = os.path.basename(experiments[i].path)
            elif name == "name":
                val = experiments[i].name
            elif name in experiments[i].params:
                val = experiments[i].params[name]
            else:
                val = experiments[i].outs[name]

            if name == "date":
                val = val.ctime()

            if type(val) == float:
                val = "{:.2f}".format(val)

            width = len(str(val))
            max   = width if width > max else max

        col_maxes.append(max)

    if interactive_mode or do_csv:
        col = 0
        for name in names:
            val_end = sep if name != names[-1] else ""
            if do_csv:
                print(name, end=val_end)
            else:
                print("{:<{}}".format(name, col_maxes[col]), end=val_end)
            col += 1
        print("")
        col = 0
        if not do_csv:
            for name in names:
                print("-" * col_maxes[col], end="  ")
                col += 1
            print("")

    for i in exp_indices:
        col = 0
        for name in names:
            if name == "hash":
                val = os.path.basename(experiments[i].path)
            elif name == "name":
                val = experiments[i].name
            elif name in experiments[i].params:
                val = experiments[i].params[name]
            else:
                val = experiments[i].outs[name]

            if name == "date":
                val = val.ctime()

            if do_csv:
                fmt_string="{}"
                if type(val) == bool:
                    val = int(val)
            else:
                fmt_string="{:<{}}"
                if type(val) == int or type(val) == float:
                    fmt_string="{:>{}}"

                if type(val) == float:
                    val = "{:.2f}".format(val)
                elif type(val) == bool:
                    val = "YES" if val else "NO"

            val_end = "" if name == names[-1] else sep
            if do_csv:
                print(fmt_string.format(val), end=val_end)
            else:
                print(fmt_string.format(val, col_maxes[col]), end=val_end)

            col += 1

        print("")

def do_show(exp_indices, *names):
    _do_show(exp_indices, False, *names)

def do_showcsv(exp_indices, *names):
    _do_show(exp_indices, True, *names)

def get_report_results(exp_indices, benchmarks, configs, iters, stat):

    res = {}
    for bench in benchmarks:
        if not bench in res:
            res[bench] = {}

        for cfg in configs:
            if not cfg in res[bench]:
                res[bench][cfg] = []

    for i in exp_indices:
        exp   = experiments[i]
        bench = exp.params["benchmark"]
        cfg   = exp.name
        it    = exp.params["iteration"]

        if not bench in res:
            continue
        if not cfg in res[bench]:
            continue
        if not it in [i for i in range(iters)]:
            continue
        if not stat in exp.outs:
            continue

        res[bench][cfg].append(exp.outs[stat])

    for bench in benchmarks:
        for cfg in configs:
            r = res[bench][cfg]
            count = len(r)
            if count > 1:
                res[bench][cfg] = (safe_mean(r), stdev(r), count)
            else:
                res[bench][cfg] = (safe_mean(r), 0.0, count)

    return res

def collect_report_strs(exp_indices, benchmarks, configs, iters, \
    base_config, stat, absolute, cis):

    res = get_report_results(exp_indices, benchmarks, configs, \
          iters, stat)

    rstrs = {}
    rstrs["average"] = {}
    for bench in benchmarks:
        if not bench in rstrs:
            rstrs[bench] = {}

        for cfg in configs:

            expval = expstd = None
            baseval = basestd = None
            ci = None
            valstr = None
            if not cfg in rstrs["average"]:
                rstrs["average"][cfg] = []

            if bench in res and cfg in res[bench]:
                expval, expstd, expcount = res[bench][cfg]

            if base_config != None:
                if bench in res and base_config in res[bench]:
                    baseval, basestd, basecount = res[bench][base_config]

                if expcount != basecount:
                    print("experiment iteration count mismatch!")

                if cis:
                    ci = get95CI(baseval, expval, basestd, expstd, expcount)

            if absolute:
                valstr = "{:.2f}".format(expval)
                if ci:
                  valstr += ("{:>6s}".format("{:.1f}".format(ci)))

                rstrs["average"][cfg].append(expval)
            else:
                relval = safe_div(expval, baseval)
                valstr = "{:.3f}".format(relval)
                if ci:
                    relci   = safe_div(ci, baseval)
                    valstr += ("{:>6s}".format("{:.3f}".format(relci)))
                rstrs["average"][cfg].append(relval)

            rstrs[bench][cfg] = valstr

    for cfg in configs:
        if absolute:
            avgval = safe_mean(rstrs["average"][cfg])
            valstr = "{:.2f}".format(avgval)
            if ci:
              valstr += ("{:>6s}".format("-"))
        else:
            avgval = safe_geo_mean(rstrs["average"][cfg])
            valstr = "{:.3f}".format(avgval)
            if ci:
              valstr += ("{:>6s}".format("-"))
        rstrs["average"][cfg] = valstr

    return rstrs

def parse_report_args(exp_indices, *args):

    benchmarks  = \
    configs     = \
    iters       = \
    base_config = \
    stat        = \
    absolute    = \
    cis         = None

    for arg in args:
        try:
            tup = tuple(arg.strip('(').strip(')').split(","))

            if tup[0] == "benchmarks":
                benchmarks = [x.strip('"') for x in tup[1:]]
            elif tup[0] == "configs":
                configs = [x.strip('"') for x in tup[1:]]
            elif tup[0] == "iters":
                iters = int(tup[1])
            elif tup[0] == "base_config":
                base_config = str(tup[1])
            elif tup[0] == "stat":
                stat = str(tup[1])
            elif tup[0] == "absolute":
                absolute = bool((tup[1]=="True"))
            elif tup[0] == "cis":
                cis = bool((tup[1]=="True"))
            else:
                raise Exception("unknown report argument: '{}'".format(tup[0]))

        except:
            err("invalid report arg '{}'".format(arg))

    if benchmarks == None:
        benchmarks = []
        [ benchmarks.append(experiments[i].params["benchmark"]) for i in exp_indices \
          if not experiments[i].params["benchmark"] in benchmarks ]
        benchmarks.sort()

    if configs == None:
        configs = []
        [ configs.append(experiments[i].name) for i in exp_indices \
          if not experiments[i].name in configs ]
        tmp_configs = []
        if ("default_32" in configs):
            tmp_configs.append("default_32")
        if ("default_30" in configs):
            tmp_configs.append("default_30")
        configs = tmp_configs + sorted([ x for x in configs  \
                                         if not x in tmp_configs ])

    if iters == None:
        iters = 1

    if base_config == None:
        base_config = configs[0]

    if stat == None:
        stat = "runtime"

    if absolute == None:
        absolute = False

    if cis == None:
        cis = False

    return (benchmarks, configs, iters, base_config, stat, absolute, cis)

def do_report(exp_indices, *args):
    if len(exp_indices) == 0:
        return

    benchmarks,  \
    configs,     \
    iters,       \
    base_config, \
    stat,        \
    absolute,    \
    cis          = parse_report_args(exp_indices, *args)

    print("Report for the following info:\n")

    print("benchmarks:  '{}'".format(benchmarks))
    print("configs:     '{}'".format(configs))
    print("iters:       '{}'".format(iters))
    print("base_config: '{}'".format(base_config))
    print("stat:        '{}'".format(stat))
    print("absolute:    '{}'".format(absolute))
    print("cis:         '{}'".format(cis))
    print("")

    rstrs = collect_report_strs(exp_indices, benchmarks, configs, iters, \
                                base_config, stat, absolute, cis)

    max_c = 2 + max([ len(n) for n in configs ])
    max_b = 2 + max([ len(n) for n in benchmarks ])

    print("{:<{}}".format("benchmark", max_b),end="")
    for cfg in configs:
        print("{:>{}}".format(cfg, max_c),end="")
    print("")

    for bench in benchmarks:
        print("{:<{}}".format(bench, max_b),end="")
        for cfg in configs:
            print("{:>{}}".format(rstrs[bench][cfg], max_c),end="")
        print("")

    print("{:<{}}".format("average", max_b),end="")
    for cfg in configs:
        print("{:>{}}".format(rstrs["average"][cfg], max_c),end="")
    print("")

def do_name(exp_indices, *args):
    if len(exp_indices) == 0:
        return

    name = args[0]
    for i in exp_indices:
        set_name(experiments[i], name)

def is_before(at, bt):
    if len(at.split()) == 1:
        at += " 23:59:59"
    elif len(bt.split()) == 1:
        bt += " 00:00:00"

    a = time.strptime(at, '%Y-%m-%d %H:%M:%S')
    b = time.strptime(bt, '%Y-%m-%d %H:%M:%S')
    return a < b

def is_after(at, bt):
    if len(at.split()) == 1:
        at += " 00:00:00"
    elif len(bt.split()) == 1:
        bt += " 23:59:59"

    a = time.strptime(at, '%Y-%m-%d %H:%M:%S')
    b = time.strptime(bt, '%Y-%m-%d %H:%M:%S')
    return a > b

def _do_diff(exp_indices, csv, outs, *show_cols):
    if len(exp_indices) == 0:
        return

    show_names = [ *show_cols ]

    plist = list(experiments[exp_indices[0]].params.keys())
    olist = list(experiments[exp_indices[0]].outs.keys())
    names = set(plist + olist)
    for i in exp_indices:
        plist = list(experiments[i].params.keys())
        olist = list(experiments[i].outs.keys())
        kset  = set(plist + olist) if outs else set(plist)
        names = names & kset
    names = sorted(list(names))

    for name in names:
        add = False
        for i in exp_indices:
            for j in exp_indices:
                if i == j:
                    continue
                if name in experiments[j].params:
                    if experiments[i].params[name] != experiments[j].params[name]:
                        add = True
                        break
                elif name in experiments[j].outs:
                    if experiments[i].outs[name] != experiments[j].outs[name]:
                        add = True
                        break
            if not add:
                break
        if add:
            show_names.append(name)

    if csv:
        do_showcsv(exp_indices, *show_names)
    else:
        do_show(exp_indices, *show_names)

def do_diff(exp_indices, *show_cols):
    _do_diff(exp_indices, False, True, *show_cols)

def do_paramdiff(exp_indices, *show_cols):
    _do_diff(exp_indices, False, False, *show_cols)

def do_diffcsv(exp_indices, *show_cols):
    _do_diff(exp_indices, True, True, *show_cols)

def _cmd_show(csv):
    load_exps()

    args     = sys.argv[2:]
    names    = []
    queries  = []

    if len(args) >= 1:
        names = args[0].split()

    if len(args) > 1:
        queries = args[1:]

    exp_indices = range(0, len(experiments))

    for q in queries:
        exp_indices = query(q, exp_indices)

    if csv:
        do_showcsv(exp_indices, *names)
    else:
        do_show(exp_indices, *names)

def cmd_show():
    _cmd_show(False)

def cmd_showcsv():
    _cmd_show(True)

def cmd_name():
    load_exps()

    args    = sys.argv[2:]
    name    = []
    queries = []

    if len(args) >= 1:
        name = args[0]

    if len(args) > 1:
        queries = args[1:]

    exp_indices = range(0, len(experiments))

    for q in queries:
        exp_indices = query(q, exp_indices)

    do_name(exp_indices, name)

def cmd_report():
    load_exps()

    args        = sys.argv[2:]
    report_args = []
    queries     = []

    if len(args) >= 1:
        report_args = args[0].split()

    if len(args) > 1:
        queries = args[1:]

    exp_indices = range(0, len(experiments))

    for q in queries:
        exp_indices = query(q, exp_indices)

    do_report(exp_indices, *report_args)


def _cmd_diff(csv, outs):
    load_exps()

    args     = sys.argv[2:]
    names    = []
    queries  = []

    if len(args) >= 1:
        names = args[0].split()

    if len(args) > 1:
        queries = args[1:]

    exp_indices = range(0, len(experiments))

    for q in queries:
        exp_indices = query(q, exp_indices)

    if csv:
        do_diffcsv(exp_indices, *names)
    elif outs:
        do_diff(exp_indices, *names)
    else:
        do_parmadiff(exp_indices, *names)

def cmd_diff():
    _cmd_diff(False, True)

def cmd_paramdiff():
    _cmd_diff(False, False)

def cmd_diffcsv():
    _cmd_diff(True, True)

def do_expr(exp_indices, *expr_strs):
    _exp_vals = {}

    _exp_vals["hash"] = []
    for _exp_idx in exp_indices:
        _exp_vals["hash"].append(os.path.basename(experiments[_exp_idx].path))
        for _key, _val in experiments[_exp_idx].params.items():
            if not _key in _exp_vals:
                _exp_vals[_key] = []
            _exp_vals[_key].append(_val)
        for _key, _val in experiments[_exp_idx].outs.items():
            if not _key in _exp_vals:
                _exp_vals[_key] = []
            _exp_vals[_key].append(_val)

    _expr = ' '.join(expr_strs)

    _fn_str = "def run_expr("
    _comma  = ""
    for _key, _vals in _exp_vals.items():
        _fn_str += _comma + _key
        _comma=", "
    _fn_str += "):\n    return "
    _fn_str += _expr

    exec(_fn_str, globals())

    print(run_expr(**_exp_vals))

def do_interactive():
    global interactive_mode
    interactive_mode = True

    try:
        get_input = raw_input
    except NameError:
        get_input = input

    console_cmds = {
        "list":      do_list,
        "show":      do_show,
        "showcsv":   do_showcsv,
        "name":      do_name,
        "report":    do_report,
        "csv":       do_csv,
        "fullcsv":   do_fullcsv,
        "diff":      do_diff,
        "paramdiff": do_paramdiff,
        "diffcsv":   do_diffcsv,
        "expr":      do_expr,
    }

    load_exps()

    def complete(text, state):
        words = { "hash": 1 }
        for exp in experiments:
            for key in exp.params.keys():
                words[key] = 1
            for key in exp.outs.keys():
                words[key] = 1
        for cmd in console_cmds.keys():
            words[cmd] = 1

        results = [ key for key in words.keys() if key.startswith(text) ] + [ None ]

        return results[state]

    readline.parse_and_bind('tab: complete')
    readline.set_completer(complete)


    exp_indices = list(range(0, len(experiments)))
    state_hist  = [ exp_indices ]

    while True:
        state_jump = False
        try:
            q = get_input("\033[34m${}\033[0m \033[32m[\033[0m{}\033[32m] > \033[0m".format(len(state_hist), len(exp_indices)))
            q = q.lstrip()

            if q.startswith("$"):
                try:
                    state       = int(q[1:])
                    exp_indices = state_hist[state - 1]
                    state_jump  = True
                except:
                    print("\033[31minvalid state '{}'\033[0m".format(q[1:]))
                    continue
            elif q.startswith(":"):
                split = q[1:].split()
                cmd   = split[0] if len(split) >= 1 else ""

                if cmd in console_cmds:
                    args = split[1:] if len(split) > 1 else []
                    try:
                        console_cmds[cmd](exp_indices, *args)
                    except Exception as E:
                        print("\033[31m{}\033[0m".format(traceback.format_exc()))
                else:
                    print("\033[31minvalid console command '{}'\033[0m".format(cmd))

                continue

        except:
            print("")
            return

        if not state_jump:
            keep = exp_indices

            try:
                keep = query(q, exp_indices)
            except BadQueryException as Q:
                print("\033[31m{}\033[0m".format(str(Q)))

            exp_indices = keep

        state_hist.append(exp_indices)

def cmd_console():
    do_interactive()

def do_cmd(cmd):
    cmds = {
        "copydir":   cmd_copydir,
        "list":      cmd_list,
        "show":      cmd_show,
        "showcsv":   cmd_showcsv,
        "name":      cmd_name,
        "report":    cmd_report,
        "csv":       cmd_csv,
        "diff":      cmd_diff,
        "paramdiff": cmd_paramdiff,
        "console":   cmd_console,
    }

    if not cmd in cmds:
        err("invalid command '{}'".format(cmd))

    cmds[cmd]()

def main():
    global rapport_dir
    rapport_dir = os.getenv("RAPPORT_DIR") or ".rapport"
    if len(rapport_dir) > 1 and rapport_dir[-1] == '/':
        rapport_dir = rapport_dir[0:-1]

    cmd = sys.argv[1] if len(sys.argv) > 1 else None

    if cmd:
        do_cmd(cmd)
    else:
        err("missing command")

if __name__ == '__main__':
    main()
